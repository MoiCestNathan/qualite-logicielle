import models.Clock;
import models.InternalClock;
import views.EnglishClock;
import views.UniversalClock;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) {
        InternalClock horlogeInterne = new InternalClock();


        EnglishClock englishClock = new EnglishClock();
        UniversalClock universalClock = new UniversalClock();

        horlogeInterne.addObserver(englishClock);
        horlogeInterne.addObserver(universalClock);

        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(2);

        scheduler.scheduleAtFixedRate(() -> {
            horlogeInterne.updateTime();
            englishClock.displayTimeEnglish();
        }, 0, 1, TimeUnit.SECONDS);

        scheduler.scheduleAtFixedRate(() -> {
            horlogeInterne.updateTime();
            universalClock.displayTimeUniversal();
        }, 0, 1, TimeUnit.MINUTES);
    }
}
