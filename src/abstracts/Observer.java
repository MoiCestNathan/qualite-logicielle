package abstracts;

import java.time.LocalDateTime;

public interface Observer {
    void update(LocalDateTime currentTime);
}


