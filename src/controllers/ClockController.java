package controllers;

import models.Clock;

import java.time.LocalDateTime;

public class ClockController {
    private Clock clock;

    public ClockController(Clock clock) {
        this.clock = clock;
    }

    public void changeTimeFormat() {
        // Changer le format de l'heure
    }

    public void adjustTime(LocalDateTime newTime) {
        clock.setTime(newTime);
    }
}

