package views;

import abstracts.Observer;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class UniversalClock implements Observer {
    private LocalDateTime currentTime;
    private static final long refreshRate = 60000; // 1 minute

    @Override
    public void update(LocalDateTime currentTime) {
        this.currentTime = currentTime;
    }

    public void displayTimeUniversal() {
        String formattedTime = formatTimeUniversal(currentTime);
        System.out.println("Heure universelle : " + formattedTime);
    }

    private String formatTimeUniversal(LocalDateTime time) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH'h'mm");
        return time.format(formatter);
    }

}
