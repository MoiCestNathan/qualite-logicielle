package views;

import abstracts.Observer;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class EnglishClock implements Observer {
    private LocalDateTime currentTime;
    private static final long refreshRate = 1000; // 1 seconde

    @Override
    public void update(LocalDateTime currentTime) {
        this.currentTime = currentTime;
    }

    public void displayTimeEnglish() {
        String formattedTime = formatTimeAngloSaxon(currentTime);
        System.out.println("Heure anglo-saxonne : " + formattedTime);
    }

    private String formatTimeAngloSaxon(LocalDateTime time) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm:ss a");
        return time.format(formatter);
    }

}
