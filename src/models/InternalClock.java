package models;

import java.time.LocalDateTime;

public class InternalClock extends Clock {

    public void updateTime() {
        // Mettre à jour l'heure à partir de l'horloge système
        this.time = LocalDateTime.now();
        notifyObservers();
    }
}
