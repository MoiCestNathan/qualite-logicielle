package models;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import abstracts.Observer;


public abstract class Clock {
    protected LocalDateTime time;
    private List<Observer> observers = new ArrayList<>();

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime newTime) {
        this.time = newTime;
        notifyObservers();
    }

    public void addObserver(Observer o) {
        observers.add(o);
    }

    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    protected void notifyObservers() {
        for (Observer o : observers) {
            o.update(time);
        }
    }
}

